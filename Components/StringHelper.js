function toLower(inputString) {
  return inputString.toLowerCase();
}

module.exports = { toLower };