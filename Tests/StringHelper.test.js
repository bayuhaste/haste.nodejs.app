const string = require("../Components/StringHelper");

test("Low case from Liberty equal to liberty", () => {
  expect(string.toLower("G3F0RCE")).toBe("g3f0rce");
  expect(string.toLower("eNTER")).toBe("enter");
  expect(string.toLower("The QUick brOWn foX JUmPs oVEr the lAzy DOG")).toBe("the quick brown fox jumps over the lazy dog");
});