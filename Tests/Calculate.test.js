const calc = require("../Components/Calculate");

test("Normal sum e.g. adds -3 + 5 to equal 2", () => {
  expect(calc.sum(3,5)).toBe(8);
  expect(calc.sum(-3,5)).toBe(2);
  expect(calc.sum(3,-5)).toBe(-2);
  expect(calc.sum(-3,-5)).toBe(-8);
});

test("Decimal sum e.g. adds 3.14159265 + -6.125 to equal -2.98340735", () => {
  expect(calc.sum(3.14159265,4.23451)).toBe(7.37610265);
  expect(calc.sum(3.14159265,-6.125)).toBe(-2.98340735);
  expect(calc.sum(-3.1415,1.1015)).toBe(-2.04);
  expect(calc.sum(-3.1415,-2.256)).toBe(-5.3975);
});

test("Normal substraction 5-3 to equal 2", () => {
  expect(calc.substract(3,5)).toBe(-2);
  expect(calc.substract(-3,5)).toBe(-8);
  expect(calc.substract(3,-5)).toBe(8);
  expect(calc.substract(-3,-5)).toBe(2);
});

test("Normal multiplication 2 * 4 to equal 8", () => {
  expect(calc.multiply(3,5)).toBe(15);
  expect(calc.multiply(-3,5)).toBe(-15);
  expect(calc.multiply(3,-5)).toBe(-15);
  expect(calc.multiply(-3,-5)).toBe(15);
});